/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examencorte1;

/**
 *
 * @author virge
 */
public class Administrativo extends Empleado {
    private int tipoContrato; // 1: base, 2: eventual

    // Constructor

    public Administrativo(int numEmpleado, String nombre, String domicilio, int pagoDia, int diasTrabajados) {
        super(numEmpleado, nombre, domicilio, pagoDia, diasTrabajados);
        this.tipoContrato = tipoContrato;

    }
   

    // Getters y Setters
    public int getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(int tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    // Métodos
    public double calcularRetencion() {
        double totalPagar = calcularTotalPagar();
        if (totalPagar < 5000) {
            return totalPagar * 0.05;
        } else if (totalPagar <= 10000) {
            return totalPagar * 0.08;
        } else {
            return totalPagar * 0.10;
        }
    }
}
