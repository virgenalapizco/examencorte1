/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examencorte1;

/**
 *
 * @author virge
 */
public class Docente extends Empleado {
    private int nivel; // 1: Pre escolar, 2: Primaria, 3: Secundaria

    // Constructor

    public Docente(int numEmpleado, String nombre, String domicilio, int pagoDia, int diasTrabajados) {
        super(numEmpleado, nombre, domicilio, pagoDia, diasTrabajados);
    }


    // Getters y Setters
    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    // Métodos
    public double calcularPagoAdicional() {
        double totalPagar = calcularTotalPagar();
        switch (nivel) {
            case 1:
                return totalPagar * 0.35;
            case 2:
                return totalPagar * 0.40;
            case 3:
                return totalPagar * 0.50;
            default:
                return 0;
        }
    }
}
