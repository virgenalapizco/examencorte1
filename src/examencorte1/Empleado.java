/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examencorte1;

/**
 *
 * @author virge
 */
public abstract class Empleado {
    protected int numEmpleado;
    protected String nombre;
    protected String domicilio;
    protected int pagoDia;
    protected int diasTrabajados;
    
    // Constructores

    public Empleado(int numEmpleado, String nombre, String domicilio, int pagoDia, int diasTrabajados) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.pagoDia = pagoDia;
        this.diasTrabajados = diasTrabajados;
    }
    
    // Encapsulamiento

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getPagoDia() {
        return pagoDia;
    }

    public void setPagoDia(int pagoDia) {
        this.pagoDia = pagoDia;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
   
    // Métodos
    public double calcularTotalPagar() {
        return pagoDia * diasTrabajados;
    }

    public double calcularDescuentoISR() {
        double porcentajeISR = 0;
        return calcularTotalPagar() * porcentajeISR / 100;
    }
    
    
}
